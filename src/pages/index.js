import * as React from "react"
import "../global.css"


const SideMenu = () => {
  return(
    <div className="navigation-bar">
      PlaceHolder
    </div>
  )
}

const RollingText = () => {
    return (
      <div className="marquee">
          <p>Git Python Java</p>
      </div>
    )
}

const Presentation = () => {
  return (
    <div>
      <h1>About me</h1>
      <p>
          I m actually a fourth year computer science student at <a href="https://www.epita.fr/">EPITA</a>
        </p>
    </div>
  )
}

const IndexPage = () => {
  return (
    <main className="page">
      <SideMenu></SideMenu>

      <div className="name-container">
        <h1 className="name">Backend Developper</h1>
        <h2 className="name"> Portfolio</h2>
      </div>
      <div>
        <RollingText></RollingText>
      </div>
      

      
    </main>
  )
}

export default IndexPage
export const Head = () => <title>Alioune</title>
